# Copyright (C) 2019 BlogOnYourOwn.com
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Boyo 2.0.1\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/style\n"
"POT-Creation-Date: 2019-09-13 17:38:24+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2019-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 0.5.4\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"

#: 404.php:19
msgid "Error 404"
msgstr ""

#: 404.php:24
msgid "It looks like nothing was found at this location."
msgstr ""

#: 404.php:25
msgid "Maybe try a search?"
msgstr ""

#: amp/footer.php:14
msgid "Back to top"
msgstr ""

#: archive.php:43 index.php:45 search.php:45
msgid "Prev"
msgstr ""

#: comments.php:60
msgid "Comments are closed."
msgstr ""

#: footer.php:22
#. translators: 1: Theme name, 2: Theme author.
msgid "Theme by %1$s"
msgstr ""

#: functions.php:48
msgid "Primary"
msgstr ""

#: functions.php:158
msgid "Top"
msgstr ""

#: functions.php:160 functions.php:171 functions.php:182
msgid "Add widgets here."
msgstr ""

#: functions.php:169
msgid "Bottom"
msgstr ""

#: functions.php:180
msgid "Footer"
msgstr ""

#: header.php:26
msgid "Skip to content"
msgstr ""

#: inc/class-tgm-plugin-activation.php:327
msgid "Install Required Plugins"
msgstr ""

#: inc/class-tgm-plugin-activation.php:328
msgid "Install Plugins"
msgstr ""

#: inc/class-tgm-plugin-activation.php:330
#. translators: %s: plugin name.
msgid "Installing Plugin: %s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:332
#. translators: %s: plugin name.
msgid "Updating Plugin: %s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:333
msgid "Something went wrong with the plugin API."
msgstr ""

#: inc/class-tgm-plugin-activation.php:370
#. translators: 1: plugin name(s).
msgid "Begin installing plugin"
msgid_plural "Begin installing plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:375
msgid "Begin updating plugin"
msgid_plural "Begin updating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:380
msgid "Begin activating plugin"
msgid_plural "Begin activating plugins"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:385
msgid "Return to Required Plugins Installer"
msgstr ""

#: inc/class-tgm-plugin-activation.php:386
#: inc/class-tgm-plugin-activation.php:827
#: inc/class-tgm-plugin-activation.php:2533
#: inc/class-tgm-plugin-activation.php:3580
msgid "Return to the Dashboard"
msgstr ""

#: inc/class-tgm-plugin-activation.php:387
#: inc/class-tgm-plugin-activation.php:3159
msgid "Plugin activated successfully."
msgstr ""

#: inc/class-tgm-plugin-activation.php:388
#: inc/class-tgm-plugin-activation.php:2952
msgid "The following plugin was activated successfully:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:390
#. translators: 1: plugin name.
msgid "No action taken. Plugin %1$s was already active."
msgstr ""

#: inc/class-tgm-plugin-activation.php:392
#. translators: 1: plugin name.
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""

#: inc/class-tgm-plugin-activation.php:394
#. translators: 1: dashboard link.
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:395
msgid "Dismiss this notice"
msgstr ""

#: inc/class-tgm-plugin-activation.php:396
msgid ""
"There are one or more required or recommended plugins to install, update or "
"activate."
msgstr ""

#: inc/class-tgm-plugin-activation.php:397
msgid "Please contact the administrator of this site for help."
msgstr ""

#: inc/class-tgm-plugin-activation.php:522
msgid "This plugin needs to be updated to be compatible with your theme."
msgstr ""

#: inc/class-tgm-plugin-activation.php:523
msgid "Update Required"
msgstr ""

#: inc/class-tgm-plugin-activation.php:934
msgid ""
"The remote plugin package does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: inc/class-tgm-plugin-activation.php:934
#: inc/class-tgm-plugin-activation.php:937
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: inc/class-tgm-plugin-activation.php:937
msgid ""
"The remote plugin package consists of more than one file, but the files are "
"not packaged in a folder."
msgstr ""

#: inc/class-tgm-plugin-activation.php:1982
#. translators: %s: version number
msgid "TGMPA v%s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2273
msgid "Required"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2276
msgid "Recommended"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2292
msgid "WordPress Repository"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2295
msgid "External Source"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2298
msgid "Pre-Packaged"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2315
msgid "Not Installed"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2319
msgid "Installed But Not Activated"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2321
msgid "Active"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2327
msgid "Required Update not Available"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2330
msgid "Requires Update"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2333
msgid "Update recommended"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2392
#. translators: 1: number of plugins.
msgid "To Install <span class=\"count\">(%s)</span>"
msgid_plural "To Install <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:2396
#. translators: 1: number of plugins.
msgid "Update Available <span class=\"count\">(%s)</span>"
msgid_plural "Update Available <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:2400
#. translators: 1: number of plugins.
msgid "To Activate <span class=\"count\">(%s)</span>"
msgid_plural "To Activate <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:2490
msgid "Installed version:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2498
msgid "Minimum required version:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2510
msgid "Available version:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2533
msgid "No plugins to install, update or activate."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2547
msgid "Plugin"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2548
msgid "Source"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2549
msgid "Type"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2553
msgid "Version"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2554
msgid "Status"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2603
#. translators: %2$s: plugin name in screen reader markup
msgid "Install %2$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2608
#. translators: %2$s: plugin name in screen reader markup
msgid "Update %2$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2614
#. translators: %2$s: plugin name in screen reader markup
msgid "Activate %2$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2684
msgid "Upgrade message from the plugin author:"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2717
msgid "Install"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2723
msgid "Update"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2726
msgid "Activate"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2757
msgid "No plugins were selected to be installed. No action taken."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2759
msgid "No plugins were selected to be updated. No action taken."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2800
msgid "No plugins are available to be installed at this time."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2802
msgid "No plugins are available to be updated at this time."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2908
msgid "No plugins were selected to be activated. No action taken."
msgstr ""

#: inc/class-tgm-plugin-activation.php:2934
msgid "No plugins are available to be activated at this time."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3158
msgid "Plugin activation failed."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3498
#. translators: 1: plugin name, 2: action number 3: total number of actions.
msgid "Updating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/class-tgm-plugin-activation.php:3501
#. translators: 1: plugin name, 2: error message.
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3503
#. translators: 1: plugin name.
msgid "The installation of %1$s failed."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3507
msgid ""
"The installation and activation process is starting. This process may take "
"a while on some hosts, so please be patient."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3509
#. translators: 1: plugin name.
msgid "%1$s installed and activated successfully."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3509
#: inc/class-tgm-plugin-activation.php:3517
msgid "Show Details"
msgstr ""

#: inc/class-tgm-plugin-activation.php:3509
#: inc/class-tgm-plugin-activation.php:3517
msgid "Hide Details"
msgstr ""

#: inc/class-tgm-plugin-activation.php:3510
msgid "All installations and activations have been completed."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3512
#. translators: 1: plugin name, 2: action number 3: total number of actions.
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/class-tgm-plugin-activation.php:3515
msgid ""
"The installation process is starting. This process may take a while on some "
"hosts, so please be patient."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3517
#. translators: 1: plugin name.
msgid "%1$s installed successfully."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3518
msgid "All installations have been completed."
msgstr ""

#: inc/class-tgm-plugin-activation.php:3520
#. translators: 1: plugin name, 2: action number 3: total number of actions.
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: inc/customizer.php:49
msgid "Main Content Background Color"
msgstr ""

#: inc/customizer.php:70
msgid "Aside Post Format Background Color"
msgstr ""

#: inc/customizer.php:91
msgid "Quote Post Format Background Color"
msgstr ""

#: inc/customizer.php:112
msgid "Link Post Format Background Color"
msgstr ""

#: inc/customizer.php:133
msgid "Active Menu Underline Color"
msgstr ""

#: inc/customizer.php:144
msgid "Blog & Archive Pages"
msgstr ""

#: inc/customizer.php:146
msgid "Blog & Archive Pages Settings"
msgstr ""

#: inc/customizer.php:161
msgid "Blog & Archive Pages Layout"
msgstr ""

#: inc/customizer.php:165
msgid "One column"
msgstr ""

#: inc/customizer.php:166
msgid "Three columns"
msgstr ""

#: inc/customizer.php:182
msgid "Show excerpt on blog archive pages"
msgstr ""

#: inc/customizer.php:183
msgid "Excerpt is always displayed for Sticky Posts"
msgstr ""

#: inc/customizer.php:200
msgid "Length of Autogenerated Excerpt (number of words)"
msgstr ""

#: inc/customizer.php:202
msgid "It works only when excerpt field in editor is empty"
msgstr ""

#: inc/customizer.php:216
msgid "Advanced"
msgstr ""

#: inc/customizer.php:218
msgid "Advanced Settings"
msgstr ""

#: inc/customizer.php:233
msgid "Load fonts from Google servers"
msgstr ""

#: inc/template-tags.php:16
msgid "Published: "
msgstr ""

#: inc/template-tags.php:19
msgid "Last Updated: "
msgstr ""

#: inc/template-tags.php:72 template-parts/content-page.php:45
#. translators: %s: Name of current post. Only visible to screen readers
msgid "Edit <span class=\"screen-reader-text\">%s</span>"
msgstr ""

#: inc/template-tags.php:240
#. translators: used between list items, there is a space after the comma
msgid ", "
msgstr ""

#: inc/template-tags.php:274
#. translators: 1: list of categories.
msgid "About authors"
msgstr ""

#: inc/template-tags.php:279
msgid "About author"
msgstr ""

#: inc/tgmpa-plugins.php:30
msgid "Jetpack by WordPress.com"
msgstr ""

#: inc/tgmpa-plugins.php:37
msgid "Contact Form 7"
msgstr ""

#: search.php:22
#. translators: %s: search query.
msgid "Search Results for: %s"
msgstr ""

#: single.php:32
msgid "Previous article"
msgstr ""

#: single.php:33
msgid "Next article"
msgstr ""

#: template-parts/content-aside.php:20 template-parts/content-quote.php:20
#: template-parts/content-single.php:40 template-parts/content.php:27
#. translators: %s: Name of current post. Only visible to screen readers
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr ""

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#: template-parts/content-none.php:24
#. translators: 1: link to WP admin new post page.
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:37
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: template-parts/content-none.php:44
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: template-parts/content-page.php:29 template-parts/content-single.php:53
msgid "Pages:"
msgstr ""

#: template-parts/navigation-amp.php:44 template-parts/navigation.php:24
msgid "Primary Menu"
msgstr ""

#: template-parts/navigation-amp.php:59 template-parts/navigation-amp.php:85
#: template-parts/navigation.php:32 template-parts/navigation.php:45
msgid "Search"
msgstr ""

#. Theme Name of the plugin/theme
msgid "Boyo"
msgstr ""

#. Theme URI of the plugin/theme
msgid "https://blogonyourown.com/themes/boyo"
msgstr ""

#. Description of the plugin/theme
msgid ""
"BOYO is another WordPress theme by BlogOnYourOwn.com. Clear, readable, "
"fully adapted to be a blog template. Minimalist and clean style makes your "
"content and photos look beautiful. Light, fast and SEO optimized. Tested "
"with new Block Editor (Gutenberg). AMP plugin supported. Try the BOYO theme "
"and enjoy its beauty."
msgstr ""

#. Author of the plugin/theme
msgid "BlogOnYourOwn.com"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://blogonyourown.com/"
msgstr ""

#: comments.php:35
#. translators: 1: comment count number
msgctxt "comments title"
msgid "%1$s comment"
msgid_plural "%1$s comments"
msgstr[0] ""
msgstr[1] ""

#: functions.php:213
#. Translators: If there are characters in your language that are not supported
#. by Montserrat, translate this to 'off'. Do not translate into your own
#. language.
msgctxt "Montserrat font: on or off"
msgid "on"
msgstr ""

#: inc/class-tgm-plugin-activation.php:1121
#: inc/class-tgm-plugin-activation.php:2948
msgctxt "plugin A *and* plugin B"
msgid "and"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2342
#. translators: 1: install status, 2: update status
msgctxt "Install/Update Status"
msgid "%1$s, %2$s"
msgstr ""

#: inc/class-tgm-plugin-activation.php:2388
#. translators: 1: number of plugins.
msgctxt "plugins"
msgid "All <span class=\"count\">(%s)</span>"
msgid_plural "All <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: inc/class-tgm-plugin-activation.php:2482
msgctxt "as in: \"version nr unknown\""
msgid "unknown"
msgstr ""